package com.machinetest.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Saurabh Godbole on 13/09/17.
 */

public class Product {


    @SerializedName("productname")
    @Expose
    private String productname;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("vendorname")
    @Expose
    private String vendorname;
    @SerializedName("vendoraddress")
    @Expose
    private String vendoraddress;
    @SerializedName("productImg")
    @Expose
    private String productImg;
    @SerializedName("productGallery")
    @Expose
    private List<String> productGallery = null;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVendorname() {
        return vendorname;
    }

    public void setVendorname(String vendorname) {
        this.vendorname = vendorname;
    }

    public String getVendoraddress() {
        return vendoraddress;
    }

    public void setVendoraddress(String vendoraddress) {
        this.vendoraddress = vendoraddress;
    }

    public String getProductImg() {
        return productImg;
    }

    public void setProductImg(String productImg) {
        this.productImg = productImg;
    }

    public List<String> getProductGallery() {
        return productGallery;
    }

    public void setProductGallery(List<String> productGallery) {
        this.productGallery = productGallery;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    @Override
    public String toString() {
        return "Product{" +
                "productname='" + productname + '\'' +
                ", price='" + price + '\'' +
                ", vendorname='" + vendorname + '\'' +
                ", vendoraddress='" + vendoraddress + '\'' +
                ", productImg='" + productImg + '\'' +
                ", productGallery=" + productGallery +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
