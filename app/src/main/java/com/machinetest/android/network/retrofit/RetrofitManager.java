package com.machinetest.android.network.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.machinetest.android.util.Constants.BASE_URL;

/**
 * Created by Saurabh Godbole on 13/09/17.
 */

public class RetrofitManager {


    //  public static final String BASE_URL = " http://jsonplaceholder.typicode.com/";
    private static Retrofit retrofit = null;

    private static Retrofit retrofitMockable = null;



    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }


    public static ApiEndpointInterface getAPIService() {

        return getClient().create(ApiEndpointInterface.class);
    }

    //For Mockabale Call


    public static Retrofit getMockableClient() {
        if (retrofitMockable==null) {
            retrofitMockable = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitMockable;
    }

    public static ApiEndpointInterface getMockableAPIService() {

        return getMockableClient().create(ApiEndpointInterface.class);
    }
}
