package com.machinetest.android.network.retrofit;

import com.machinetest.android.model.ProductList;
import com.machinetest.android.util.Constants;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Saurabh Godbole on 13/09/17.
 */

public interface ApiEndpointInterface {


    @GET(Constants.GET_DATA)
    Call<ProductList> getAllProducts();
}
