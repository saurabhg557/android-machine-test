package com.machinetest.android.app;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

/**
 * Created by Saurabh Godbole on 9/15/2017.
 */

public class MyApplication extends Application{

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }

    @Override
    public void onCreate() {
        super.onCreate();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
}
