package com.machinetest.android.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.machinetest.android.model.Product;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Saurabh Godbole on 9/14/2017.
 */

public class SharedPrefsUtil {

private static final String TAG = "SharedPrefsUtil";


    public static void putString(Context context, String prefName,
                                 String key, String value) {
        try {
            SharedPreferences preferences = context.getSharedPreferences(prefName,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(key, value);
            editor.apply();
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static String getString(Context context,
                                   String prefName, String key) {
        String toReturn = "";
        try {
            SharedPreferences preferences = context.getSharedPreferences(prefName,
                    Context.MODE_PRIVATE);
            toReturn = preferences.getString(key, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toReturn;
    }





    public static void putObject(Context context, String prefName, String key, HashMap<String, Product> object) {
        try {
            SharedPreferences preferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            Gson gson = new Gson();
            String value = gson.toJson(object);
            editor.putString(key, value);
            editor.apply();
            editor.commit();
            Log.d(TAG, "putObject: "+ object.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public static HashMap<String, Product> getObject(Context context, String prefName, String key) {
        HashMap<String, Product> toReturn = null;
        try {
            SharedPreferences preferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
            Gson gson = new Gson();
            String value = preferences.getString(key, "");
            Type stringStringMap = new TypeToken<HashMap<String, Product>>(){}.getType();
            toReturn = gson.fromJson(value,stringStringMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toReturn;
    }





    public static void clearAllPrefs(Context context) {

        SharedPreferences preferences = context.getSharedPreferences(Constants.CART_PREFERENCE, Context.MODE_PRIVATE);

        SharedPreferences.Editor prefsEditor = preferences.edit();
        // clear all the flags except the device reset flag
        prefsEditor.clear();

        prefsEditor.commit();

    }
}
