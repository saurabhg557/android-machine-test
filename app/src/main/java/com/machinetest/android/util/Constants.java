package com.machinetest.android.util;

/**
 * Created by Saurabh Godbole on 13/09/17.
 */

public class Constants {

    public static final String BASE_URL = "http://mobiletest-hackathon.herokuapp.com/";

    public static final String GET_DATA = BASE_URL+"getdata/";

    public static final String CART_PREFERENCE = "cart_preference";

    public static final String CART_PRODUCTS_KEY = "cart_products";
    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;
}
