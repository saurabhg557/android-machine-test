package com.machinetest.android.feature.shop;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.machinetest.android.R;
import com.machinetest.android.model.Product;
import com.machinetest.android.util.Constants;
import com.machinetest.android.util.SharedPrefsUtil;

import java.util.HashMap;

public class ShopActivity extends AppCompatActivity {

    private ProductsFragment mProductsFragment;
    private CartFragment mCartFragment;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            FragmentManager fragmentManager;

            switch (item.getItemId()) {
                case R.id.navigation_home:
                  loadProductsFragment();
                    return true;
                case R.id.navigation_dashboard:
                  loadCartFragment();
                    return true;

            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        getSupportActionBar().hide();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mProductsFragment = new ProductsFragment();
        mCartFragment = new CartFragment();

        loadProductsFragment();


    }





    /**
     * load Products fragment
     */
    private void loadProductsFragment() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, mProductsFragment)
                .commit();

    }


    /**
     * load Cart fragment
     */
    private void loadCartFragment() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content, mCartFragment)
                .commit();

    }


    /**
     * only add to cart if not previously added
     * @param product
     */
    public void addToCart(Product product) {

        HashMap<String, Product> productHashMap =  SharedPrefsUtil.getObject(ShopActivity.this, Constants.CART_PREFERENCE,Constants.CART_PRODUCTS_KEY);


        if(productHashMap == null)
         productHashMap = new HashMap<>();


        if (productHashMap.get(product.getProductname()) != null) {

            Toast.makeText(this, "Product already in Cart !", Toast.LENGTH_SHORT).show();

        } else {

            productHashMap.put(product.getProductname(), product);
            // No such key
            SharedPrefsUtil.putObject(ShopActivity.this, Constants.CART_PREFERENCE,Constants.CART_PRODUCTS_KEY,productHashMap);

            Toast.makeText(this, "Product Added in Cart.", Toast.LENGTH_SHORT).show();

        }



    }

    /**
     * delete product from cart
     * @param product
     */
    public void deleteFromCart(Product product) {


      HashMap<String, Product> productHashMap =  SharedPrefsUtil.getObject(ShopActivity.this, Constants.CART_PREFERENCE,Constants.CART_PRODUCTS_KEY);

      productHashMap.remove(product.getProductname());

      SharedPrefsUtil.putObject(ShopActivity.this, Constants.CART_PREFERENCE,Constants.CART_PRODUCTS_KEY,productHashMap);


    }


    /**
     * get products from cart
     * @return
     */
    public HashMap<String, Product> getCart() {

        HashMap<String, Product> productHashMap = SharedPrefsUtil.getObject(ShopActivity.this, Constants.CART_PREFERENCE,Constants.CART_PRODUCTS_KEY);

        if(productHashMap == null) {

            productHashMap = new HashMap<>();
        }

        return productHashMap;
    }


}
