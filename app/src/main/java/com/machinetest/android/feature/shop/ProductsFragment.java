package com.machinetest.android.feature.shop;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.machinetest.android.R;
import com.machinetest.android.model.ProductList;
import com.machinetest.android.network.NetworkManager;
import com.machinetest.android.network.retrofit.ApiEndpointInterface;
import com.machinetest.android.network.retrofit.RetrofitManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Saurabh Godbole on 13/09/17.
 */

public class ProductsFragment extends Fragment {


    private final String TAG = "ProductsFragment";

    private RecyclerView mProductsRecyclerView;
    private TextView mNoInternetTextView;
    private ProgressBar mProgressBar;
    private ProductsAdapter mProductsAdapter;

    private ApiEndpointInterface apiEndpointInterface;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiEndpointInterface= RetrofitManager.getAPIService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_products, container, false);
        mProductsRecyclerView =  view.findViewById(R.id.rv_products);
        mNoInternetTextView = view.findViewById(R.id.tv_no_internet);
        mProgressBar = view.findViewById(R.id.progressbar);
        mProductsRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));


        mProductsAdapter = new ProductsAdapter(getActivity(), new ProductList());
        mProductsRecyclerView.setAdapter(mProductsAdapter);

        fetchProductsAPI();


        return view;
    }


    private void fetchProductsAPI() {

        if (NetworkManager.isNetworkAvailable(getActivity())) {

            mProgressBar.setVisibility(View.VISIBLE);


            Call<ProductList> call = apiEndpointInterface.getAllProducts();
            call.enqueue(new Callback<ProductList>() {
                @Override
                public void onResponse(Call<ProductList> call, Response<ProductList> response) {

                    mProductsRecyclerView.setVisibility(View.VISIBLE);
                    mNoInternetTextView.setVisibility(View.GONE);

                    mProgressBar.setVisibility(View.GONE);

                    ProductList productList = response.body();
                    Log.d(TAG, "onResponse: "+productList.toString());

                    mProductsAdapter.setProductList(productList);

                }

                @Override
                public void onFailure(Call<ProductList> call, Throwable t) {

                    mProgressBar.setVisibility(View.GONE);

                    Log.d(TAG, "onFailure: "+ t.getMessage().toString());

                }
            });

        }

        else {

            mProductsRecyclerView.setVisibility(View.GONE);
            mNoInternetTextView.setVisibility(View.VISIBLE);


        }
    }
}
