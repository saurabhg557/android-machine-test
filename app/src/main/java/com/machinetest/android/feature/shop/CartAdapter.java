package com.machinetest.android.feature.shop;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.machinetest.android.R;
import com.machinetest.android.model.ProductList;
import com.squareup.picasso.Picasso;

/**
 * Created by Saurabh Godbole on 13/09/17.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    private Context context;
    private ProductList productList;
    private ShopActivity shopActivity;
    private CartFragment cartFragment;


    public CartAdapter(Context context, ProductList productList, CartFragment cartFragment) {
        this.context = context;
        shopActivity = (ShopActivity) context;
        this.productList = productList;
        this.cartFragment = cartFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {



        holder.productNameTextView.setText(productList.getProducts().get(position).getProductname());
        holder.productPriceTextView.setText("Price\n"+productList.getProducts().get(position).getPrice());
        holder.vendorName.setText(productList.getProducts().get(position).getVendorname());
        holder.vendorAddress.setText(productList.getProducts().get(position).getVendoraddress());

        Picasso.with(context)
                .load(productList.getProducts().get(position).getProductImg())
               // .placeholder(R.drawable.ic_image_black_24dp)
              //  .error(R.drawable.ic_image_black_24dp)
                .fit()
                .centerCrop()
                .into(holder.productImageView);

        holder.removeFromCartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                shopActivity.deleteFromCart(productList.getProducts().get(position));

                productList.getProducts().remove(position);

                notifyDataSetChanged();

                Toast.makeText(context, "Product Removed from Cart", Toast.LENGTH_SHORT).show();

                if(getItemCount() == 0)
                    cartFragment.showEmptyCart();

                cartFragment.showPrice(productList);

            }
        });

        holder.callVendorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cartFragment.callVendor(productList.getProducts().get(position).getPhoneNumber());


            }
        });


    }

    @Override
    public int getItemCount() {
        if(productList!=null  && productList.getProducts()!=null)
            return productList.getProducts().size();
        else
            return 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        View mView;
        TextView productNameTextView;
        TextView productPriceTextView;
        ImageView productImageView;
        TextView vendorName;
        TextView vendorAddress;
        Button callVendorButton;
        Button removeFromCartButton;

        MyViewHolder(View view) {
            super(view);
            mView = view;


            productNameTextView = mView.findViewById(R.id.tv_product_name);
            productPriceTextView = mView.findViewById(R.id.tv_product_price);
            productImageView = mView.findViewById(R.id.iv_product);
            vendorName = mView.findViewById(R.id.tv_vendor_name);
            vendorAddress = mView.findViewById(R.id.tv_vendor_Address);
            callVendorButton = mView.findViewById(R.id.bn_call_vendor);
            removeFromCartButton = mView.findViewById(R.id.bn_remove_from_cart);
        }

    }

    public ProductList getProductList() {
        return productList;
    }

    public void setProductList(ProductList productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }
}
