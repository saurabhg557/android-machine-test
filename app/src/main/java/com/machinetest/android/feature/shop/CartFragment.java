package com.machinetest.android.feature.shop;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.machinetest.android.R;
import com.machinetest.android.model.Product;
import com.machinetest.android.model.ProductList;
import com.machinetest.android.util.Constants;
import com.machinetest.android.util.PermissionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by Saurabh Godbole on 13/09/17.
 */

public class CartFragment extends Fragment {


    private FrameLayout containerLayout;
    private RecyclerView mCarRecyclerView;
    private CartAdapter mCartAdapter;
    private ProductList productList;
    private ArrayList<Product> products;
    private TextView emptyCartTextView;
    private TextView totalPriceTextView;

    private ShopActivity shopActivity;

    private String phone;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_cart, container, false);


        containerLayout = view.findViewById(R.id.container);
        mCarRecyclerView = view.findViewById(R.id.rv_cart);
        emptyCartTextView = view.findViewById(R.id.tv_empty_cart);
        totalPriceTextView = view.findViewById(R.id.tv_total_price);
        mCarRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

       shopActivity = (ShopActivity) getActivity();
       HashMap<String, Product> productHashMap = shopActivity.getCart();

      //  printMap(productHashMap);

        Log.d(TAG, "in cart: "+productHashMap.toString());
        products = new ArrayList<>(productHashMap.values());
        Log.d(TAG, "in cart list : "+products.toString());

        if(products.size() == 0) {

            emptyCartTextView.setVisibility(View.VISIBLE);
            containerLayout.setVisibility(View.GONE);
        }
        else {
            emptyCartTextView.setVisibility(View.GONE);
            containerLayout.setVisibility(View.VISIBLE);
        }


        productList = new ProductList();
        productList.setProducts(products);



        mCartAdapter = new CartAdapter(getActivity(), productList, CartFragment.this);
        mCarRecyclerView.setAdapter(mCartAdapter);

        showPrice(productList);

        return view;
    }

    public static void printMap(Map mp) {
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            Log.d(TAG, "printMap: "+pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }
    }


    public void showEmptyCart() {

        emptyCartTextView.setVisibility(View.VISIBLE);
        containerLayout.setVisibility(View.GONE);
    }


    public void showPrice(ProductList productList) {



        int totalPrice = 0;

        for(int i=0; i<productList.getProducts().size(); i++) {

            totalPrice = totalPrice + Integer.parseInt(productList.getProducts().get(i).getPrice());

        }


        totalPriceTextView.setText("Total Price : Rs. "+totalPrice);


    }



    public void callVendor(String phone) {

     startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null)));

    }





/** Not Required
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if(phone!=null)
                     callVendor(phone);

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    */
}
